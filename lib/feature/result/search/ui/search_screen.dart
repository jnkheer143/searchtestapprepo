import 'package:flutter/material.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/ui/result_search_list.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/ui/search_bar.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Search App"),
        ),
        body: Column(
          children: <Widget>[SearchBar(), SearchListScreen()],
        ),

    );
  }
}
