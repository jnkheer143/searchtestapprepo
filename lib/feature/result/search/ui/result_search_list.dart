import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_lyrics/feature/result/details/result_details_screen.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_bloc.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_event.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_state.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result.dart';
import 'package:flutter_bloc_lyrics/resources/langs/strings.dart';

class SearchListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainSearchBloc, MainSearchState>(
      bloc: BlocProvider.of<MainSearchBloc>(context),
      builder: (BuildContext context, MainSearchState state) {
        if (state is SearchStateLoading) {
          return Padding(
              padding: EdgeInsets.only(top: 16.0),
              child:Center(child: CircularProgressIndicator()));
        }
        if (state is SearchStateError) {
          return Text(state.error);
        }
        if (state is SearchStateSuccess) {
          return state.pageList.isEmpty
              ? Text('No result found ')
              : Expanded(
                  child: _SearchResults(
                    pageList: state.pageList,
                  ),
                );
        } else {
          return Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: Text('Enter text and search on wikipedia'));
        }
      },
    );
  }
}

class _SearchResults extends StatelessWidget {
  final List<Pages> pageList;

  const _SearchResults({Key key, @required this.pageList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: pageList.length,
        itemBuilder: (BuildContext context, int index) {
          return _SongSearchResultItem(
            song: pageList[index],
          );
        });
  }
}

class _SongSearchResultItem extends StatelessWidget {
  final Pages song;

  const _SongSearchResultItem({Key key, this.song}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return song.thumbnail != null
        ? _getSongDetailsLayout(context)
        : Dismissible(
            background: Container(
              color: Colors.red,
            ),
            onDismissed: (direction) {
              BlocProvider.of<MainSearchBloc>(context)
                  .add(RemoveResult(resultID: song.pageid));
            },
            key: Key(UniqueKey().toString()),
            child: _getSongDetailsLayout(context));
  }

  Padding _getSongDetailsLayout(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        child: ListTile(
          leading: song.thumbnail == null
              ? Icon(Icons.sd_card)
              : Image.network(
                  song.thumbnail.source,
                ),
          title: Text(song.title),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailsScreen(song)));
          },
        ));
  }
}
