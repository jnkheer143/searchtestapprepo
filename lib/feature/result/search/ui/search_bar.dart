import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_bloc.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_event.dart';
import 'package:flutter_bloc_lyrics/resources/langs/strings.dart';

class SearchBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final _SearchController = TextEditingController();
  MainSearchBloc _songSearchBloc;

  @override
  void initState() {
    super.initState();
    _songSearchBloc = BlocProvider.of<MainSearchBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _SearchController,
      onChanged: (text) {
        _songSearchBloc.add(TextChanged(query: text));
      },
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.search),
          hintText: 'Enter text..'),
    );
  }

  @override
  void dispose() {
    _SearchController.dispose();
    super.dispose();
  }
}
