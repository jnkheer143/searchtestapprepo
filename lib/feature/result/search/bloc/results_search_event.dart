import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result.dart';

abstract class MainSearchEvent extends Equatable {
  MainSearchEvent([List props = const []]) : super(props);
}

class TextChanged extends MainSearchEvent {
  final String query;

  TextChanged({this.query}) : super([query]);

  @override
  String toString() => "SearchTextChanged { query: $query }";
}

class RemoveResult extends MainSearchEvent {
  final int resultID;

  RemoveResult({this.resultID}) : super([resultID]);

  @override
  String toString() => "Remove result { resultID: $resultID }";
}


