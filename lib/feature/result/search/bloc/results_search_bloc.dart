import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc_lyrics/common/constants.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_event.dart';
import 'package:flutter_bloc_lyrics/feature/result/search/bloc/results_search_state.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result_error.dart';
import 'package:flutter_bloc_lyrics/repository/results_repository.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

class MainSearchBloc extends Bloc<MainSearchEvent, MainSearchState> {
  final ResultsRepository resultRepository;

  StreamSubscription addEditBlocSubscription;

  MainSearchBloc(
      {@required this.resultRepository,}) {

  }

  @override
  MainSearchState get initialState => SearchStateEmpty();


  @override
  Stream<Transition<MainSearchEvent, MainSearchState>> transformEvents(Stream<MainSearchEvent> events,
      TransitionFunction<MainSearchEvent, MainSearchState> transitionFn) {
    final nonDebounceStream =
        events.where((event) => event is! TextChanged);

    final debounceStream =
    events.where((event) => event is TextChanged).debounceTime(
      Duration(milliseconds: DEFAULT_SEARCH_DEBOUNCE),
    );

    return super.transformEvents(
        MergeStream([nonDebounceStream, debounceStream]), transitionFn);
  }

  @override
  Stream<MainSearchState> mapEventToState(MainSearchEvent event) async* {
    if (event is TextChanged) {
      yield* _mapSongSearchTextChangedToState(event);
    }
    if (event is RemoveResult) {
      yield* _mapSongRemoveToState(event);
    }
    
  }

  Stream<MainSearchState> _mapSongSearchTextChangedToState(
      TextChanged event) async* {
    final String searchQuery = event.query;
    if (searchQuery.isEmpty) {
      yield SearchStateEmpty();
    } else {
      yield SearchStateLoading();
      try {
        final result = await resultRepository.searchResult(searchQuery);
        yield SearchStateSuccess(result, searchQuery);
      } catch (error) {
        yield error is SearchResultError
            ? SearchStateError(error.message)
            : SearchStateError("Default error");
      }
    }
  }

  Stream<MainSearchState> _mapSongRemoveToState(RemoveResult event) async* {
    await resultRepository.removeResult(event.resultID);
    if (state is SearchStateSuccess) {
      SearchStateSuccess searchState = state;
      searchState.pageList.removeWhere((song) {
        return song.pageid == event.resultID;
      });
      yield SearchStateSuccess(searchState.pageList, searchState.query);
    }
  }


  @override
  void onTransition(Transition<MainSearchEvent, MainSearchState> transition) {
    print(transition);
  }

  @override
  Future<void> close() {
    addEditBlocSubscription.cancel();
    return super.close();
  }
}
