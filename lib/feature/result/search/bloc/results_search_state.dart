import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result.dart';

abstract class MainSearchState extends Equatable {
  MainSearchState([List props = const []]) : super(props);
}

class SearchStateEmpty extends MainSearchState {
  @override
  String toString() => 'SearchStateEmpty';
}

class SearchStateLoading extends MainSearchState {
  @override
  String toString() => 'SearchStateLoading';
}

class SearchStateSuccess extends MainSearchState {
  final List<Pages> pageList;
  final String query;

  SearchStateSuccess(this.pageList, this.query) : super([pageList]);

  @override
  String toString() => 'SearchStateSuccess { songs: ${pageList.length} }';
}

class SearchStateError extends MainSearchState {
  final String error;

  SearchStateError(this.error) : super([error]);

  @override
  String toString() => 'SearchStateError { error: $error }';
}
