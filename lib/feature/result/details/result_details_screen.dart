import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result.dart';

class DetailsScreen extends StatelessWidget {
  final Pages page;

  DetailsScreen(this.page);

  @override
  Widget build(BuildContext context) {
    print("fff`${page.thumbnail}");
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
      ),
      body: page!=null?Container(
        padding: EdgeInsets.all(12),
        child: Column(
          children: [
            page.thumbnail==null?Container():page!=null&&page.thumbnail != null&&page.thumbnail.source!=null&&page.thumbnail.source!=''
                ? Image.network(
                    page.thumbnail.source,
                    height: page.thumbnail.height.toDouble(),
                    width: page.thumbnail.width.toDouble(),
                  )
                : Continue(),
            Padding(
              padding: const EdgeInsets.only(top: 12),
              child: Text(page.title),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12),
              child: Text(page.terms.description[0]),
            ),
          ],
        ),
      ):Container(),
    );
  }
}
