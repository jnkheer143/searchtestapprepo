import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_bloc_lyrics/model/api/search_result.dart';
import 'package:flutter_bloc_lyrics/model/api/search_result_error.dart';
import 'package:http/http.dart' as http;

class ResultClient {
  final http.Client httpClient;


  ResultClient({httpClient, localClient})
      : this.httpClient = httpClient ?? http.Client();

  Future<Query> searchResult(String query) async {
    final response = await httpClient.get(
      Uri.parse("https://en.wikipedia.org/w/api.php?action=query&format=json&prop=pageimages%7Cpageterms&generator=prefixsearch&redirects=1&formatversion=2&piprop=thumbnail&pithumbsize=50&pilimit=10&wbptterms=description&gpssearch=$query&gpslimit=10"),
    );
    final results = json.decode(response.body);

    if (response.statusCode >= 200 && response.statusCode < 300) {
      return SearchResult.fromJson(results).query;
    } else {
      throw MetaResponse.fromJson(results).searchResultError;
    }
  }
}
