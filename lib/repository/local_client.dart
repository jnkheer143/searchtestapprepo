import 'package:flutter_bloc_lyrics/model/api/search_result.dart';

//Mocked repository which works as remote client
class LocalClient {
  final List<Pages> localResultsList = List();




  Future<List<Pages>> getResults(String query) async {
    await Future.delayed(Duration(milliseconds: 1000));
    return localResultsList;


  }

  Future<void> removeResult(int localID) async {
    await Future.delayed(Duration(milliseconds: 1000));
    localResultsList.removeAt(localID);
  }
}
