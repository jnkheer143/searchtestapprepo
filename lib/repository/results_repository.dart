import 'package:flutter_bloc_lyrics/model/api/search_result.dart';
import 'package:flutter_bloc_lyrics/repository/local_client.dart';
import 'package:flutter_bloc_lyrics/repository/resuls_client.dart';

class ResultsRepository {
  final ResultClient client;
  final LocalClient localClient;

  ResultsRepository(this.client, this.localClient);

  Future<List<Pages>> searchResult(String query) async {

    final resultAPI = await client.searchResult(query);
    final resultLocal = await localClient.getResults(query);
    resultLocal.addAll(resultAPI.pages);
    return resultLocal;
  }

  Future<void> removeResult(int songIndex) async {
    await localClient.removeResult(songIndex);
  }




}
