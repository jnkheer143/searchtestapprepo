import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_lyrics/repository/local_client.dart';
import 'package:flutter_bloc_lyrics/repository/resuls_client.dart';
import 'package:flutter_bloc_lyrics/repository/results_repository.dart';

import 'feature/result/search/bloc/results_search_bloc.dart';
import 'feature/result/search/ui/search_screen.dart';

void main() {
  final ResultsRepository _lyricsRepository =
      ResultsRepository(ResultClient(), LocalClient());

  runApp(EasyLocalization(
      path: 'assets/translations',
      supportedLocales: [Locale('en', 'US')],
      child: SearchResultApp(resultRepository: _lyricsRepository)));
}

class SearchResultApp extends StatelessWidget {
  final ResultsRepository resultRepository;

  const SearchResultApp({Key key, @required this.resultRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [

          BlocProvider<MainSearchBloc>(
            create: (context) => MainSearchBloc(
                resultRepository:  resultRepository,
                ),
          ),
        ],
        child: MaterialApp(
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            theme: ThemeData(primaryColor: Colors.blue),
            home: SearchScreen()));
  }
}
